# Shadows
Last update: 22-10-2018

## Description
Demo program for creating shadows in 2D graphics using SFML 2.3.2 library

## Software
Visual Studio 2015

## Program Operation
https://www.youtube.com/watch?v=WOMePTMr60I

## License
Creative Commons Attribution-NonCommercial-ShareAlike 4.0
